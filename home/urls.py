from django.conf.urls import include,url

from . import views

urlpatterns = [
    url('main/', views.main, name='main'),
    url('cep_data/', views.cep_data, name='cep_data'),
    url('plant_data/', views.plant_data, name='plant_data'),
    url('sl_case/', views.sl_case, name='sl_case'),
    url('attachments/', views.attachments, name='attachments'),
    url('claim_info/', views.claim_info, name='claim_info'),
    url('claim_info_table/', views.claim_info_table, name='claim_info_table'),
    url('davie_logs/', views.davie_logs, name='davie_logs'),
    url('fault_codes/', views.fault_codes, name='davie_logs'),
    url('timeline_chart/', views.timeline_chart, name='timeline_chart'),
]