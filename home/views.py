# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader

# Create your views here.
def main(request):
    template = loader.get_template('main.html')
    return HttpResponse(template.render())

def cep_data(request):
    template = loader.get_template('cep_data.html')
    return HttpResponse(template.render())
    
def plant_data(request):
    template = loader.get_template('plant_data.html')
    return HttpResponse(template.render())

def sl_case(request):
    template = loader.get_template('sl_case.html')
    return HttpResponse(template.render())

def attachments(request):
    template = loader.get_template('attachments.html')
    return HttpResponse(template.render())

def claim_info(request):
    template = loader.get_template('claim_info.html')
    return HttpResponse(template.render())

def claim_info_table(request):
    template = loader.get_template('claim_info_table.html')
    return HttpResponse(template.render())

def davie_logs(request):
    template = loader.get_template('davie_logs.html')
    return HttpResponse(template.render())
    
def fault_codes(request):
    template = loader.get_template('fault_codes.html')
    return HttpResponse(template.render())   

def timeline_chart(request):
    template = loader.get_template('timeline_chart.html')
    return HttpResponse(template.render())